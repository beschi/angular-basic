var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    less = require('gulp-less'),
    LessPluginCleanCSS = require('less-plugin-clean-css'),
    cleanCss = new LessPluginCleanCSS({advanced: true}),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create();
require('minifyify');

gulp.task('bundle-js', function() {
    return browserify('./app/app.js')
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./target/'));
});

gulp.task('bundle-css', function() {
    return gulp.src('./app/app.less')
        .pipe(less())
        .pipe(gulp.dest('./target/'));
});

//create the app js and css file bundles
gulp.task('bundle-app', ['bundle-js', 'bundle-css']);

// start the server and reload if there are any changes
gulp.task('reload', ['bundle-app'], function() {
    browserSync.reload();
});
gulp.task('start', ['bundle-app'], function() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch(['app/**/*.*'], ['reload']);
});

// min version
gulp.task('bundle-js-min', function() {
    return browserify({debug: true})
        .add('./src/app.js')
        .plugin('minifyify', {
            map: 'app.map.json',
            output: './js/app.map.json'
        })
        .bundle()
        .pipe(source('app.min.js'))
        .pipe(gulp.dest('./js/'));
});

gulp.task('bundle-css-min', function() {
    return gulp.src('./src/app.less')
        .pipe(less({
            plugins: [cleanCss]
        }))
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('./css/'));
});

// create minified version of js and css bundle
gulp.task('bundle-app-min', ['bundle-js-min', 'bundle-css-min']);

// do everything
gulp.task('bundle-all', ['bundle-app', 'bundle-app-min']);
