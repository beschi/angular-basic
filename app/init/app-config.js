module.exports = function ($routeProvider) {
    $routeProvider.
        when('/home', {
            controller: 'HomeController',
            templateUrl: 'app/templates/home.html'
        }).
        when('/cities/:id', {
            controller: 'CityListController',
            templateUrl: 'app/templates/cities.html'
        }).
        when('/city/:id', {
            contoller: 'CityDetails',
            templateUrl: 'app/templates/city-details.html'
        }).
        when('/cities', {redirectTo: '/cities/0'}).
        otherwise({redirectTo: '/home'});
};
