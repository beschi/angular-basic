module.exports = function ($rootScope) {
    function resetUser() {
        $rootScope.userLoggedIn = false;
        $rootScope.superUser = false;
    }
    $rootScope.pageName = {
        HOME: "home",
        CITIES: "cities",
        CITY_INFO: "cityInfo",
        LOGIN: "login"
    };
    $rootScope.cities = {};
    $rootScope.updateCityNamesForSearch = function (cityArray) {
        this.cities = cityArray;
    };

    $rootScope.currentPage = $rootScope.pageName.HOME;
    $rootScope.$watch("currentPage", function (newValue) {
        $rootScope.$emit(newValue);
    });
    $rootScope.$watch("currentCity", function () {
        if ($rootScope.currentPage === $rootScope.pageName.CITY_INFO) {
            //used to capture the search within city details page
            $rootScope.$emit("cityInfoChange");
        }
    });

    $rootScope.doLogin = function () {
        $rootScope.currentPage = $rootScope.pageName.LOGIN;
    };
    $rootScope.doLogout = function () {
        resetUser();
    };
    resetUser();
    /* For testing purpose only */
//        $rootScope.userLoggedIn = true;
//        $rootScope.superUser = true;
    /* For testing purpose only */
};
