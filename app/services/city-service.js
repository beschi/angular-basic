module.exports = function ($log) {
    var defaultList = [
        {"id": 1, "catId": 1, "name": "Goa", "state": "Goa", "image": ""},
        {"id": 2, "catId": 1, "name": "Kovalam", "state": "Kerala", "image": ""},
        {"id": 3, "catId": 2, "name": "Agra", "state": "Uttar Pradesh", "image": ""},
        {"id": 4, "catId": 2, "name": "Mysore", "state": "Karnataka", "image": ""},
        {"id": 5, "catId": 2, "name": "Madurai", "state": "Tamilnadu", "image": ""},
        {"id": 6, "catId": 3, "name": "Ooty", "state": "Tamilnadu", "image": ""}
    ],
            currentList,
            currentContext = 'CityService: ',
            currentCategory = 0,
            categoryCount = {};
    /**
     * To update the city count in each category
     */
    function updateCategoryCount() {
        categoryCount = {};
        angular.forEach(currentList, function (value) {
            if (categoryCount[value.catId]) {
                categoryCount[value.catId]++;
            } else {
                categoryCount[value.catId] = 1;
            }
        });
    }
    this.initialiseData = function () {
        currentList = defaultList;
        updateCategoryCount();
        // console.log(categoryCount);
    };
    this.getCountOfCategory = function (catId) {
        return categoryCount[catId] || 0;
    };
    /**
     * To get the list of cities - if no category selected, returns complete list
     * If selected, filters the city only reltated to the particular category
     * @method getList
     * @returns {Array}
     */
    this.getList = function () {
        var currentCategoryList = [];
        // console.log(currentCategory);
        if (currentCategory) {
            angular.forEach(currentList, function (value) {
                if (value.catId === currentCategory) {
                    currentCategoryList.push(value);
                }
            });
            return currentCategoryList;
        } else {
            return currentList;
        }
    };
    this.setCurrentCategory = function (id) {
        currentCategory = id || 0;
    };
    /**
     * Finds and returns the city by its id
     * @param {Number/String} id
     * @returns {Object}
     */
    this.getCityById = function (id) {
        for (var i = 0; i < currentList.length; i++) {
            if (String(currentList[i].id) === String(id)) {
                return currentList[i];
                break;
            }
        }
        return 0;
    };
    /**
     * To get the city name from city id
     * @method getCityNameById
     * @param {Number/String} id
     * @returns {Stinrg}
     */
    this.getCityNameById = function (id) {
        var city = this.getCityById(id);
        if (city) {
            return city.name;
        }
        return 0;
    };
    /**
     * Finds and returns the id of the city by name
     * @param {String} name
     * @returns {Number}
     */
    this.getCityIdByName = function (name) {
        for (var i = 0; i < currentList.length; i++) {
            if (String(currentList[i].name) === String(name)) {
                return currentList[i].id;
                break;
            }
        }
        return 0;
    };
    /**
     * Adds a city name in the list
     * @param {String} name
     * @param {Number/String} catId
     */
    this.addCity = function (name, catId) {
        var lastCityId = currentList[currentList.length - 1].id;
        catId = Number(catId);
        currentList.push({"id": lastCityId + 1, "catId": catId, "name": name});
        updateCategoryCount();
    };
    /**
     * Update operation of city - by id
     * @param {Number} id
     * @param {String} name
     * @param {Number} catId
     */
    this.updateCity = function (id, name, catId) {
        catId = Number(catId);
        var city = this.getCityById(id);
        if (city) {
            city.name = name;
            city.catId = catId;
            updateCategoryCount();
        }
    };
    this.deleteCity = function (id) {
        for (var i = 0; i < currentList.length; i++) {
            if (String(currentList[i].id) === String(id)) {
                currentList.splice(i, 1);
                updateCategoryCount();
                break;
            }
        }
    };
    /**
     * Returns the arracy which contains only the city names
     * @returns {Array}
     */
    this.getCityNamesArray = function () {
        var i,
                cityArray = [];
        for (i = 0; i < currentList.length; i++) {
            cityArray.push(currentList[i].name);
        }
        return cityArray;
    };
};
