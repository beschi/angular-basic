module.exports = function ($log, CityService) {
    var defaultList = [
        {"id": 1, "name": "Beaches"},
        {"id": 2, "name": "Heritage"},
        {"id": 3, "name": "Hills"},
        {"id": 4, "name": "Leisure"}
    ],
            currentList,
            currentContext = 'CategoryService: ',
            i;
    this.initialiseData = function () {
        currentList = defaultList;
    };
    this.getList = function () {
        angular.forEach(currentList, function (value) {
            value.cityCount = CityService.getCountOfCategory(value.id);
        });
        return currentList;
    };
    this.getCategoryById = function (id) {
        for (i = 0; i < currentList.length; i++) {
            if (currentList[i].id === id) {
                return currentList[i];
            }
        }
        return 0;
    };
    this.addCategory = function (name) {
        var lastCatId = currentList[currentList.length - 1].id;
        currentList.push({"id": lastCatId + 1, "name": name});
    };
    this.updateCategory = function (id, name) {
        var catToEdit = this.getCategoryById(id);
        if (catToEdit) {
            catToEdit.name = name;
        }
    };
};
