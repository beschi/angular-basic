/*global app, angular, $log*/

/**
 * To handle user accounts
 */
app.service('AccountService', [
    '$log',
    function ($log) {
        var users = [
            {"username": "admin@travelguide.com", "password": "letadmin", superUser: true},
            {"username": "watcher@travelguide.com", "password": "letwatcher", superUser: false}
        ],
                i,
                currentContext = 'AccountService: ';
        this.loginUser = function (un, pw) {
            $log.log(currentContext + 'Logging in...');
            var response = {
                loginSuccess: false,
                message: "Invalid User"
            };
            for (i = 0; i < users.length; i++) {
                if (users[i].username === un) {
                    if (users[i].password === pw) {
                        response = {
                            loginSuccess: true,
                            message: "Successfully logged in",
                            superUser: users[i].superUser
                        };
                    } else {
                        response = {
                            loginSuccess: false,
                            message: "Invalid password"
                        };
                    }
                    break;
                }
            }
            return response;
        };
    }
]);
