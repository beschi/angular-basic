/*global app, angular, $log*/

/**
 * Handles store/retrive data from localStorage
 */
app.service('StorageService', [
    function () {
        this.storageKeys = {
            CATEGORY: "categoryList",
            CITY: "cityList"
        };
        this.saveToLocalStorage = function (key, object) {
            localStorage[key] = JSON.stringify(object);
        };
        this.getFromLocalStorage = function (key) {
            return JSON.parse(localStorage[key]);
        };
    }
]);

/**
 * To handle user accounts
 */
app.service('AccountService', [
    '$log',
    function ($log) {
        var users = [
            {"username": "admin@travelguide.com", "password": "letadmin", superUser: true},
            {"username": "watcher@travelguide.com", "password": "letwatcher", superUser: false}
        ],
                i,
                currentContext = 'AccountService: ';
        this.loginUser = function (un, pw) {
            $log.log(currentContext + 'Logging in...');
            var response = {
                loginSuccess: false,
                message: "Invalid User"
            };
            for (i = 0; i < users.length; i++) {
                if (users[i].username === un) {
                    if (users[i].password === pw) {
                        response = {
                            loginSuccess: true,
                            message: "Successfully logged in",
                            superUser: users[i].superUser
                        };
                    } else {
                        response = {
                            loginSuccess: false,
                            message: "Invalid password"
                        };
                    }
                    break;
                }
            }
            return response;
        };
    }
]);


app.service('CategoryService', [
    'StorageService',
    '$log',
    'CityService',
    function (StorageService, $log, CityService) {
        var defaultList = [
            {"id": 1, "name": "Beaches"},
            {"id": 2, "name": "Heritage"},
            {"id": 3, "name": "Hills"},
            {"id": 4, "name": "Leisure"}
        ],
                currentList,
                categoryKey = StorageService.storageKeys.CATEGORY,
                currentContext = 'CategoryService: ',
                i;
        this.initialiseData = function () {
            if (!localStorage[categoryKey]) {
                $log.info(currentContext + 'Fresh Visit');
                $log.log(currentContext + 'Initialising Category default data to localStorage');
                StorageService.saveToLocalStorage(categoryKey, defaultList);
                currentList = defaultList;
            } else {
                $log.log(currentContext + 'localStorage already available...');
                currentList = StorageService.getFromLocalStorage(categoryKey);
            }
        };
        this.getList = function () {
            angular.forEach(currentList, function (value) {
                value.cityCount = CityService.getCountOfCategory(value.id);
            });
            return currentList;
        };
        this.getCategoryById = function (id) {
            for (i = 0; i < currentList.length; i++) {
                if (currentList[i].id === id) {
                    return currentList[i];
                }
            }
            return 0;
        };
        this.addCategory = function (name) {
            var lastCatId = currentList[currentList.length - 1].id;
            currentList.push({"id": lastCatId + 1, "name": name});
            // StorageService.saveToLocalStorage(categoryKey, currentList);
        };
        this.updateCategory = function (id, name) {
            var catToEdit = this.getCategoryById(id);
            if (catToEdit) {
                catToEdit.name = name;
                // StorageService.saveToLocalStorage(categoryKey, currentList);
            }
        };
    }
]);

app.service('CityService', [
    'StorageService',
    '$log',
    function (StorageService, $log) {
        var defaultList = [
            {"id": 1, "catId": 1, "name": "Goa", "state": "Goa", "image": ""},
            {"id": 2, "catId": 1, "name": "Kovalam", "state": "Kerala", "image": ""},
            {"id": 3, "catId": 2, "name": "Agra", "state": "Uttar Pradesh", "image": ""},
            {"id": 4, "catId": 2, "name": "Mysore", "state": "Karnataka", "image": ""},
            {"id": 5, "catId": 2, "name": "Madurai", "state": "Tamilnadu", "image": ""},
            {"id": 6, "catId": 3, "name": "Ooty", "state": "Tamilnadu", "image": ""}
        ],
                currentList,
                cityKey = StorageService.storageKeys.CITY,
                currentContext = 'CityService: ',
                currentCategory = 0,
                categoryCount = {};
        /**
         * To update the city count in each category
         */
        function updateCategoryCount() {
            categoryCount = {};
            angular.forEach(currentList, function (value) {
                if (categoryCount[value.catId]) {
                    categoryCount[value.catId]++;
                } else {
                    categoryCount[value.catId] = 1;
                }
            });
        }
        this.initialiseData = function () {
            if (!localStorage[cityKey]) {
                $log.log(currentContext + 'Initialising city default data to localStorage');
                StorageService.saveToLocalStorage(cityKey, defaultList);
                currentList = defaultList;
            } else {
                $log.log(currentContext + 'localStorage already available...');
                currentList = StorageService.getFromLocalStorage(cityKey);
            }
            updateCategoryCount();
            // console.log(categoryCount);
        };
        this.getCountOfCategory = function (catId) {
            return categoryCount[catId] || 0;
        };
        /**
         * To get the list of cities - if no category selected, returns complete list
         * If selected, filters the city only reltated to the particular category
         * @method getList
         * @returns {Array}
         */
        this.getList = function () {
            var currentCategoryList = [];
            // console.log(currentCategory);
            if (currentCategory) {
                angular.forEach(currentList, function (value) {
                    if (value.catId === currentCategory) {
                        currentCategoryList.push(value);
                    }
                });
                return currentCategoryList;
            } else {
                return currentList;
            }
        };
        this.setCurrentCategory = function (id) {
            currentCategory = id || 0;
        };
        /**
         * Finds and returns the city by its id
         * @param {Number/String} id
         * @returns {Object}
         */
        this.getCityById = function (id) {
            for (var i = 0; i < currentList.length; i++) {
                if (String(currentList[i].id) === String(id)) {
                    return currentList[i];
                    break;
                }
            }
            return 0;
        };
        /**
         * To get the city name from city id
         * @method getCityNameById
         * @param {Number/String} id
         * @returns {Stinrg}
         */
        this.getCityNameById = function (id) {
            var city = this.getCityById(id);
            if (city) {
                return city.name;
            }
            return 0;
        };
        /**
         * Finds and returns the id of the city by name
         * @param {String} name
         * @returns {Number}
         */
        this.getCityIdByName = function (name) {
            for (var i = 0; i < currentList.length; i++) {
                if (String(currentList[i].name) === String(name)) {
                    return currentList[i].id;
                    break;
                }
            }
            return 0;
        };
        /**
         * Adds a city name in the list
         * @param {String} name
         * @param {Number/String} catId
         */
        this.addCity = function (name, catId) {
            var lastCityId = currentList[currentList.length - 1].id;
            catId = Number(catId);
            currentList.push({"id": lastCityId + 1, "catId": catId, "name": name});
            updateCategoryCount();
            // StorageService.saveToLocalStorage(cityKey, currentList);
        };
        /**
         * Update operation of city - by id
         * @param {Number} id
         * @param {String} name
         * @param {Number} catId
         */
        this.updateCity = function (id, name, catId) {
            catId = Number(catId);
            var city = this.getCityById(id);
            if (city) {
                city.name = name;
                city.catId = catId;
                updateCategoryCount();
            }
        };
        this.deleteCity = function (id) {
            for (var i = 0; i < currentList.length; i++) {
                if (String(currentList[i].id) === String(id)) {
                    currentList.splice(i, 1);
                    updateCategoryCount();
                    break;
                }
            }
        };
        /**
         * Returns the arracy which contains only the city names
         * @returns {Array}
         */
        this.getCityNamesArray = function () {
            var i,
                    cityArray = [];
            for (i = 0; i < currentList.length; i++) {
                cityArray.push(currentList[i].name);
            }
            return cityArray;
        };
    }
]);

app.service('CityDetailsService', [
    'StorageService',
    '$log',
    'CityService',
    function (StorageService, $log, CityService) {
        //key in the object is the city id
        var defaultCityDetails = {
            "1": {
                "description": "Goa is a state located in the western region of India, it is bounded by the state of Maharashtra to the north, and by Karnataka to the east and south, while the Arabian Sea forms its western coast. It is India's smallest state by area and the fourth smallest by population. Goa is one of India's richest states with a GDP per capita two and a half times that of the country as a whole.[3] It was ranked the best placed state by the Eleventh Finance Commission for its infrastructure and ranked on top for the best quality of life in India by the National Commission on Population based on the 12 Indicators.<br>Panaji is the state's capital, while Vasco da Gama is the largest city. The historic city of Margao still exhibits the cultural influence of the Portuguese, who first landed in the early 16th century as merchants and conquered it soon thereafter. Goa is a former Portuguese province; the Portuguese overseas territory of Portuguese India existed for about 450 years until it was annexed by India in 1961.<br>Goa is visited by large numbers of international and domestic tourists each year for its beaches, places of worship and world heritage architecture. It also has rich flora and fauna, owing to its location on the Western Ghats range, which is classified as a biodiversity hotspot.",
                "image": "images/goa/goa.jpg",
                "info": [
                    {"title": "state", "value": "Goa"},
                    {"title": "Region", "value": "Western India"},
                    {"title": "Established", "value": "30 May 1987"},
                    {"title": "Total Area", "value": "3,702 km2"},
                    {"title": "Population", "value": "1,457,723"}
                ],
                "places": [
                    {
                        "name": "Baga Beach",
                        "description": "One of the most sought after beaches in India, Baga Beach is the centre of tourist attraction in Goa. Situated in North Goa, pleasant ambience, scenic views of the shoreline and plenitude of things to do makes this fishing village one of the finest places to visit in Goa. On any visit to this sandy retreat, you can witness innumerable fishing boats docked by the shoreline.",
                        "image": "images/goa/Baga_Beach.jpg"
                    },
                    {
                        "name": "Aguada Beach",
                        "description": "Known for its Aguada Fort, Aguada Beach is among the places to visit in Goa. The seashore is positioned in North Goa and is perfect to bask in beachy allures. This destination stands apart with the many historical attractions. It also allures with pristine landscape and the background of ancient citadels. Thanks to the great settings, the beach is also a favourite among honeymoon goers.",
                        "image": "images/goa/Aguada_Beach.jpg"
                    },
                    {
                        "name": "Arambol Beach",
                        "description": "Permeating a distinct bohemian vibe, Arambol Beach is said to be the most beautiful in the state. It lies on the north of Keri Beach and south of Mandrem Beach. This is also a traditional fisherman hamlet.",
                        "image": "images/goa/Arambol_Beach.jpg"
                    }
                ]
            },
            "2": {
                "description": "Kovalam is a beach town by the Arabian Sea in Thiruvananthapuram city, Kerala, India, located around 16 km from the city center.<br/>Kovalam first received attention when the Regent Maharani Sethu Lakshmi Bayi of Travancore constructed her beach resort, Halcyon Castle, here towards the end of the 1920s. Thereafter the place was brought to the public eye by her nephew the Maharaja of Travancore.[2] The European guests of the then Travancore kingdom discovered the potentiality of Kovalam beach as a tourist destination in the 1930s. However, Kovalam shot into limelight in the early seventies with arrivals of the masses of hippies on their way to Ceylon in the Hippie Trail. This exodus started the transformation of a casual fishing village of Kerala into one of the most important tourist destinations in all India.",
                "image": "images/kovalam/kovalam.jpg",
                "info": [
                    {"title": "state", "value": "Kerala"},
                    {"title": "Region", "value": "South India"},
                    {"title": "Established", "value": "30 May 1987"},
                    {"title": "Language", "value": "Malayalam"},
                    {"title": "Population", "value": "1,457,723"}
                ],
                "places": [
                    {
                        "name": "Lighthouse Beach",
                        "description": "As the name suggests, the Lighthouse beach is characterized by its 35 meter tall Lighthouse and is one of the major tourist hotspots in the city. The beach is the largest of the 3 beaches in Kovalam and is frequented mostly by the foreign tourists due to it being more developed. The Lighthouse is placed atop a hillock called the Karumkal Hill and can be easily reached by foot.",
                        "image": "images/kovalam/lighthouse-beach.jpg"
                    },
                    {
                        "name": "Hawah Beach",
                        "description": "Quiet interestingly, the Hawah Beach was named during the time when it got famous as the 1st and only topless beach India. The name Hawah translates to ‘the wind’ and is a pun on the topless European women that used to roam on the beach. Sadly (for men), topless bathing is banned now, but the beach still retains its natural beauty which is also a nice thing.",
                        "image": "images/kovalam/hawah-beach.jpg"
                    },
                    {
                        "name": "Samudra Beach",
                        "description": "Samundra beach, unlike the other two is the most sparsely populated beach in Kovalam and is mostly used by fishermen. The beach is best for people who like to spend some quiet time and meditate amidst the sounds of the sea. Bathing alone is not advisable as the beach is often vacant and the shore is a bit rocky.",
                        "image": "images/kovalam/samudra-beach.jpg"
                    },
                    {
                        "name": "Karamana River",
                        "description": "Starting near the Western Ghats, flowing into the Arabian Sea, the Karamana River is one of the major rivers in Trivandrum with two dams built on it. The river is one of the best places to visit in Kovalam to experience the fruits of nature that Kerala has been blessed with.",
                        "image": "images/kovalam/karamana-river.jpg"
                    }
                ]
            },
            "6": {
                "description": "Udhagamandalam (sometimes Ootacamund (About this sound listen (help·info))), sometimes abbreviated Udhagai and better known as Ooty (About this sound listen (help·info)), is a town, a municipality, and the district capital of the Nilgiris district in the Indian state of Tamil Nadu. It is located 80 km north of Coimbatore. It is a popular hill station located in the Nilgiri Hills. Originally occupied by the Todas, the area came under the rule of the East India Company at the end of the 18th century. Today, the town's economy is based on tourism and agriculture, along with manufacture of medicines and photographic film. The town is connected to the rest of India by road and rail, and its historic sites and natural beauty attract tourists.[4] As of 2011, the town had a population of 88,430.",
                "image": "images/ooty/ooty.jpg",
                "info": [
                    {"title": "District", "value": "The Nilgiris"},
                    {"title": "state", "value": "Tamilnadu"},
                    {"title": "Region", "value": "South India"},
                    {"title": "Elevation", "value": "2,240 m (7,350 ft)"},
                    {"title": "Language", "value": "Tamil, Badaga"},
                    {"title": "Population", "value": "88,430"},
                    {"title": "PIN", "value": "643 001"},
                    {"title": "Tele", "value": "91423"},
                    {"title": "Vehicle Registration", "value": "TN 43"},
                    {"title": "Climate", "value": "Subtropical Highland"}
                ],
                "places": [
                    {
                        "name": "Botanical Gardens",
                        "description": "Laid out in 1848, the government Botanical Gardens is located at the slopes of the Doddabetta peak. The terraced garden is maintained by the Tamil nadu horticultural society and covers an area of 22 hectares. The best time to visit the place is anytime except the monsoons. Now this place will be the first thing the people would mention when you visit ooty, and well, in my opinion it is pretty awesome. Now be prepared for a long walk when you visit this place, I would advise you not to get any kids or people who can’t walk for long. The place is amazing for photographers and casual visitors alike get a supply of food and drinks or you can also buy some snacks at the gardens. I visited this place because of my photographer friends and was totally amazed due to the amount of greens and flowers around me under an amazingly cool weather.",
                        "image": "images/ooty/botanical-gardens.jpg"
                    },
                    {
                        "name": "Tea Factory",
                        "description": "Small and nice, tea factory that is frequented by tourists who visit ooty, although not much to do here, but it was quiet a new experience to me when I visited the place. You can smell the aroma of the fresh tea leaves as soon as you near the place. I love how machines process the fresh tea leaves and package it. There’s also a shop where you can buy the tea processed at the factory. They also provided tea prepared from the freshly processed leaves and it tastes amazingly fragrant. This is a good place to show to the kids as well and I’m sure they’d be delighted.",
                        "image": "images/ooty/tea-factory.jpg"
                    },
                    {
                        "name": "Doddabetta Peak",
                        "description": "A visit to Doddabetta Peak was the reason I went to Ooty in the first place. I had heard a lot about the picturesque Nilgiris and the amazing views this place has to offer. The place stands at a height of around 8,650 feet and makes it the highest peaks in south India. This place offers amazing trek spots and plenty of quiet and picturesque locations to solitary wanderers like me. You might want to pack some really good pair of binoculars when you reach this place to enjoy it to the fullest. There is a small eatery placed here courtesy of the tourism department of Tamil nadu that offers some okay tasting food at a cheap price.",
                        "image": "images/ooty/doddabetta-peak.jpg"
                    },
                    {
                        "name": "Avalanche Lake",
                        "description": "The reason I came to ooty was to actually visit the aforementioned Doddabetta peak but wasn’t quite satisfied with amount of indulgence I was seeking with the nature. Although It was suggested by a local there and wasn’t on my itinerary, this place made me love my visit to ooty. Located in the heart of the Nilgiris 28 kilometers from ooty, Avalanche lake is certainly a wanderer’s haven. The surrounding landscape is simply mesmerizing. You can camp beside the lake in tents and enjoy trout fishing. There’s also a hatchery located nearby where you can get the fishing rod and other fishing equipment at. It’s an amazing trek location that made me stay here for 2 days at this place. The place derives its name from a landslide that occurred sometime in the early 1800’s.",
                        "image": "images/ooty/avalanche-lake.jpg"
                    },
                    {
                        "name": "Pykara River",
                        "description": "Known as a sacred place for the Toda people, Pykara River flows through the plateau and form several cascades and waterfalls. The Pykara falls are one of the most popular tourist attractions in Ooty and are famous for the shola trees, Toda settlements and wildlife.",
                        "image": "images/ooty/pykara-waterfall.jpg"
                    }
                ]
            }
        },
        currentList = defaultCityDetails,
                defaultSelection = "1",
                currentCity = 0;
        this.setCurrentCity = function (id) {
            currentCity = id || 0;
        };
        this.setDefaultCity = function () {
            currentCity = 1;
        };
        this.getCurrentCityName = function () {
            return CityService.getCityNameById(currentCity);
        };
        this.getCityInfo = function () {
            if (currentCity) {
                return currentList[currentCity];
            } else {
                return currentList[defaultSelection];
            }
        };
    }
]);
