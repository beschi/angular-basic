var angular = require('angular');
var ngRoute = require('angular-route');
var ngBootstrap = require('angular-ui-bootstrap');

var app = angular.module('travelApp', [ngRoute, ngBootstrap]);

// init
app.run(['$rootScope', require('./init/app-run.js')]);
app.config(['$routeProvider', require('./init/app-config.js')]);

// services
app.service('CityService', [
    '$log',
    require('./services/city-service.js')
]);
app.service('CityDetailsService', [
    '$log', 'CityService',
    require('./services/city-details-service')
]);
app.service('CategoryService', [
    '$log', 'CityService',
    require('./services/category-service.js')
]);

// controllers
app.controller('NavController', [
    '$rootScope', '$scope', 'CityService', 'CityDetailsService',
    require('./controllers/nav-controller.js')
]);
app.controller('HomeController', [
    '$rootScope', '$scope', 'CategoryService', 'CityService',
    require('./controllers/home-controller.js')
]);
app.controller('CityListController', [
    '$scope', '$rootScope', '$routeParams', 'CategoryService', 'CityService', 'CityDetailsService',
    require('./controllers/city-list-controller.js')
]);
app.controller('CityDetailsController', [
    '$scope', '$rootScope', '$routeParams', 'CityDetailsService',
    require('./controllers/city-details-controller.js')
]);
