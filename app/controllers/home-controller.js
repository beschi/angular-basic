module.exports = function ($rootScope, $scope, CategoryService, CityService) {
    CategoryService.initialiseData();
    CityService.initialiseData();
    $rootScope.updateCityNamesForSearch(CityService.getCityNamesArray());
    $scope.category = {};
    $rootScope.currentPage = $rootScope.pageName.HOME;

    function reset() {
        $scope.categories = CategoryService.getList();
        $scope.cancelEdit();
    }
    $rootScope.$on('home', function () {
        reset();
    });
    $scope.addOrUpdateCategory = function () {
        if ($scope.EditMode) {
            CategoryService.updateCategory($scope.category.id, $scope.category.name);
        } else {
            CategoryService.addCategory($scope.category.name);
        }
        reset();
    };
    $scope.editCategory = function ($event, catId) {
        $event.stopPropagation();
        var catToEdit = CategoryService.getCategoryById(catId);
        $scope.formMode = "Edit";
        $scope.EditMode = true;
        $scope.category.id = catToEdit.id;
        $scope.category.name = catToEdit.name;
    };
    $scope.cancelEdit = function () {
        $scope.EditMode = false;
        $scope.formMode = "Add";
        $scope.category.name = "";
        $scope.category.id = "";
    };
    reset();

    $scope.gotoCitiesOfCategory = function (catId) {

        CityService.setCurrentCategory(catId);
    };
};
