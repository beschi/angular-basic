module.exports = function ($rootScope, $scope, CityService, CityDetailsService) {
    $scope.gotoHome = function () {
        $rootScope.currentPage = $rootScope.pageName.HOME;
    };
    $scope.gotoCities = function () {
        CityService.setCurrentCategory(0);
        $rootScope.currentPage = $rootScope.pageName.CITIES;
    };
    $scope.searchResult = function (selectedTerm) {
        var cityId = CityService.getCityIdByName(selectedTerm);
        $scope.searchTerm = "";
        CityDetailsService.setCurrentCity(cityId);
        $rootScope.currentPage = $rootScope.pageName.CITY_INFO;
        $rootScope.currentCity = cityId;
    };
};
