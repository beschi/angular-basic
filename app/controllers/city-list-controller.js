module.exports = function ($scope, $rootScope, $routeParams, CategoryService, CityService, CityDetailsService) {
    $scope.city = {};
    CityService.setCurrentCategory(Number($routeParams.id));
    $rootScope.currentPage = $rootScope.pageName.CITIES;

    function reset() {
        if ($rootScope.userLoggedIn) {
            $scope.categories = CategoryService.getList();
        }
        $scope.cities = CityService.getList();
        $scope.cancelEdit();
    }
    $rootScope.$on('cities', function () {
        reset();
    });

    $scope.addOrUpdateCity = function () {
        if ($scope.EditMode) {
            CityService.updateCity($scope.city.id, $scope.city.name, $scope.city.category);
        } else {
            CityService.addCity($scope.city.name, $scope.city.category);
        }
        reset();
    };
    $scope.editCity = function ($event, cityId) {
        $event.stopPropagation();
        var cityToEdit = CityService.getCityById(cityId);
        $scope.formMode = "Edit";
        $scope.EditMode = true;
        $scope.city.id = cityToEdit.id;
        $scope.city.name = cityToEdit.name;
        $scope.city.category = String(cityToEdit.catId);
    };
    $scope.deleteCity = function ($event, cityId) {
        $event.stopPropagation();
        var deleteConfirm = confirm("Are you sure want to delete?");
        if (deleteConfirm) {
            CityService.deleteCity(cityId);
            $scope.categories = CategoryService.getList();
        }
    };

    $scope.cancelEdit = function () {
        $scope.EditMode = false;
        $scope.formMode = "Add";
        $scope.city.name = "";
        $scope.city.id = "";
        $scope.city.category = "";
    };
    reset();
    $scope.gotoCityDetails = function (cityId) {
        CityDetailsService.setCurrentCity(cityId);
        $rootScope.currentPage = $rootScope.pageName.CITY_INFO;
    };
};
