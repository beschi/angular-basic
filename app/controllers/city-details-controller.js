module.exports = function ($scope, $rootScope, $routeParams, CityDetailsService) {
    $scope.cityDetails = {};
    CityDetailsService.setCurrentCity(Number($routeParams.id));
    $rootScope.currentPage = $rootScope.pageName.CITY_INFO;

    function changeCityDetails() {
        $scope.cityDetails = CityDetailsService.getCityInfo();
        $scope.cityName = CityDetailsService.getCurrentCityName();
        //temporary fix for unavailable city
        if (!$scope.cityDetails) {
            CityDetailsService.setDefaultCity();
            $rootScope.$emit('cityInfoChange');
        }
    }
    $rootScope.$on('cityInfo', changeCityDetails);
    $rootScope.$on('cityInfoChange', changeCityDetails);
};
